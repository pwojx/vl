# MQTT alarm station

#### About this project
This project is based on ESP8266:<br />
https://github.com/esp8266/Arduino

and uses the following libraries:<br />
https://github.com/knolleary/pubsubclient<br />
https://github.com/arduino-libraries/NTPClient<br />
https://github.com/adafruit/Adafruit_NeoPixel

The main goal of this project is to provide a simple way to inform the user when a door is opened in a room by emitting light and sound. The idea is to use a [Sonoff Basic](https://sonoff.tech/product/wifi-diy-smart-switches/basicr2) smart Wi-Fi switch with [Tasmota](https://github.com/arendst/Tasmota) firmware and a door-mounted reed switch connected to it to detect when the door is opened. Upon detecting an event of the door being opened, Sonoff publishes an MQTT message to the local [broker](https://mosquitto.org/). The "vl" device subscribes to the same topic and handles incoming messages by flashing LEDs and activating a buzzer. The exact behavior of the alarm (LEDs color, duration, number of repetitions, ...) can be customized to some extent.

Besides that, it is also possible to send other messages to other supported topics (all are listed below) in order to trigger a specific behavior of the alarm. This leaves a chance to use it for other purposes as well. The device also publishes some status information to the following topics: `vl/stat`, `vl/info`, `vl/door`.

#### Electrical schematic
![](vl_schematic.png)

#### Available commands
|         Topic        |                               Message                                |                                        Description                                       |
|----------------------|:--------------------------------------------------------------------:|:----------------------------------------------------------------------------------------:|
|`vl/neopixel/enable`  |`0` or `1`                                                            |Enables or disables the LEDs.                                                             |
|`vl/neopixel/color`   |a valid HEX color, ex. `#ff1abc`                                      |Lights all LEDs in the given HEX color.                                                   |
|`vl/neopixel/sflash`  |a valid HEX color, ex. `#ff1abc`                                      |Flashes all LEDs in the given HEX color for 1 second.                                     |
|`vl/neopixel/mflash`  |a valid HEX color, ex. `#ff1abc`                                      |Flashes all LEDs in the given HEX color for 3 seconds.                                    |
|`vl/neopixel/lflash`  |a valid HEX color, ex. `#ff1abc`                                      |Flashes all LEDs in the given HEX color for 5 seconds.                                    |
|`vl/neopixel/flash`   |a valid HEX color, ex. `#ff1abc`                                      |Flashes all LEDs in the given HEX color for 1 second (alias for: `.../sflash`).           |
|`vl/neopixel/blink`   |a valid HEX color and a duration in milliseconds, ex. `#ff1abc 2000`  |Blinks all LEDs in the given HEX color for the given time.                                |
|`vl/neopixel/set`     |an index of the LED to light up and a valid HEX color, ex. `2 #ff1abc`|Lights the given LED in the given HEX color.                                              |
|`vl/neopixel/off`     |-----                                                                 |Turns off the NeoPixel LEDs.                                                              |
|`vl/buzzer/enable`    |`0` or `1`                                                            |Enables or disables the buzzer.                                                           |
|`vl/buzzer/beep`      |a duration in milliseconds                                            |Beeps the buzzer for the given time.                                                      |
|`vl/alarm/enable`     |`0` or `1`                                                            |Enables or disables the open-door alarm.                                                  |
|`vl/alarm/color1`     |a valid HEX color, ex. `#ff1abc`                                      |Sets the color 1 of the alarm.                                                            |
|`vl/alarm/color2`     |a valid HEX color, ex. `#ff1abc`                                      |Sets the color 2 of the alarm.                                                            |
|`vl/alarm/color3`     |a valid HEX color, ex. `#ff1abc`                                      |Sets the color 3 of the alarm (not used).                                                 |
|`vl/alarm/repetitions`|a number of alarm repetitions, ex. `3`                                |Sets the number of alarm repetitions (cycles).                                            |
|`vl/alarm/interval`   |an interval in milliseconds, ex. `1000`                               |Sets the duration of a single alarm cycle.                                                |
|`vl/alarm/trigger`    |`opendoor` or `closedoor`                                             |Trigger the alarm (sent automatically by the door sensor).                                |
|`vl/opendoor/enable`  |`0` or `1`                                                            |Enables or disables the open-door indicator (the LEDs can remain lit if the door is open).|
|`vl/opendoor/color`   |a valid HEX color, ex. `#ff1abc`                                      |Sets the color of the open-door indicator.                                                |
|`vl/config/load`      |-----                                                                 |Loads the configuration from EEPROM.                                                      |
|`vl/config/store`     |-----                                                                 |Stores the configuration in EEPROM.                                                       |
|`vl/stat/interval`    |an interval in milliseconds                                           |Specifies how often the device will send status information.                              |
|`vl/sleep`            |a duration in milliseconds                                            |Makes the device do nothing for the given time.                                           |

#### The name
"vl" stands for "volcano lamp" as the final product looks a bit like a volcano (especially when the LEDs are red).
<p>
    <img align="left" width="300" height="400" src="vl_image.png">
</p>
