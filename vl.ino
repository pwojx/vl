#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

#include "credentials.h"

// Buzzer
#define BUZZ_PIN 2
// NeoPixel
#define NP_PIN 0
// Number of NeoPixel LEDs
#define NP_NUM 8

// WiFi credentials and MQTT broker address
// (Define the following values in the "credentials.h"
// file or put them here directly)
const char* ssid = WIFI_SSID;
const char* passwd = WIFI_PASSWORD;
const char* broker = MQTT_BROKER;

// MQTT topics that the device needs to subscribe to
const char* topics[] = {
  "vl/neopixel/enable",
  "vl/neopixel/color",
  "vl/neopixel/sflash",
  "vl/neopixel/mflash",
  "vl/neopixel/lflash",
  "vl/neopixel/flash",
  "vl/neopixel/blink",
  "vl/neopixel/set",
  "vl/neopixel/off",
  "vl/buzzer/enable",
  "vl/buzzer/beep",
  "vl/alarm/enable",
  "vl/alarm/color1",
  "vl/alarm/color2",
  "vl/alarm/color3",
  "vl/alarm/repetitions",
  "vl/alarm/interval",
  "vl/alarm/trigger",
  "vl/opendoor/enable",
  "vl/opendoor/color",
  "vl/config/load",
  "vl/config/store",
  "vl/stat/interval",
  "vl/sleep"
};
// MQTT topics that the device publishes to
const char* stat_topic = "vl/stat";
const char* info_topic = "vl/info";
const char* door_topic = "vl/door";

// Milliseconds since the last status message
unsigned long last_stat = 0;

// Configuration variables (with default values)
struct configuration {
  // Enable flags
  bool neopixel_en = true;
  bool buzzer_en = true;
  bool alarm_en = true;
  bool indicator_en = true;

  // Colors
  char color1[8] = "#ff0000";
  char color2[8] = "#0000ff";
  char color3[8] = "#000000";
  char indicator_color[8] = "#111111";

  // Alarm config
  unsigned int alarm_repetitions = 3;     // The number of repetitions of the alarm procedure 
  unsigned int alarm_interval = 500;      // The delay between changes in alarm states

  // Status messages config
  unsigned long stat_interval = 900000;   // Milliseconds until the next status message
} cfg;

WiFiClient espClient;
PubSubClient mqttClient(espClient);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "tempus1.gum.gov.pl", 3600, 43200000); // GMT+1, update every 12 h
Adafruit_NeoPixel np(NP_NUM, NP_PIN, NEO_GRB + NEO_KHZ800);

// Gets the current timestamp and formats it properly.
// Returns a String with the formatted date and time.
String get_current_datetime() {
  unsigned int etime = timeClient.getEpochTime();
  struct tm* ptm = gmtime((time_t*)&etime);
  char buf[32] = {0};
  strftime(buf, sizeof(buf)/sizeof(buf[0]), "[%c] ", ptm);
  // [%c] is equivalent to [%a %b %e %H:%M:%S %Y]
  // ex. [Wed Dec 23 11:48:03 2020]

  return String(buf);
}

// A wrapper for the mqttClient.publish() method that
// adds the date and time to the published message.
void publish_with_datetime(const char* topic, String text) {
  String msg = get_current_datetime() + text;
  mqttClient.publish(topic, msg.c_str());
}

// Sets the NeoPixel LEDs to the color specified by r, g, b values.
void np_set_color(byte r, byte g, byte b) {
  for (int i = 0; i < NP_NUM; ++i) {
    np.setPixelColor(i, np.Color(r, g, b));
  }
  np.show();
}

// Sets the NeoPixel LEDs to the color specified by a HEX value.
// Requires a seven-character string with a valid HEX color (ex. "#ff1abc").
void np_set_color(const char* color) {
  char temp[3] = {0};
  strncpy(temp, &color[1], 2);
  byte r = strtoul(temp, NULL, 16);
  strncpy(temp, &color[3], 2);
  byte g = strtoul(temp, NULL, 16);
  strncpy(temp, &color[5], 2);
  byte b = strtoul(temp, NULL, 16);

  np_set_color(r, g, b);
}

// Turns off the NeoPixel LEDs.
void np_clear() {
  np.clear();
  np.show();
}

// Turns on the buzzer.
void buzzer_on() {
  digitalWrite(BUZZ_PIN, LOW);
}

// Turns off the buzzer.
void buzzer_off() {
  digitalWrite(BUZZ_PIN, HIGH);
}

// A callback function for "vl/neopixel/enable" topic.
// Sets the value of `cfg.neopixel_en` and/or publishes it to `info_topic`.
void cb_vl_neopixel_enable(byte* payload, unsigned int len) {
  if (len == 1) {
    if ((char)payload[0] == '0') {
      cfg.neopixel_en = false;
      publish_with_datetime(stat_topic, "NeoPixel: disabled");
      np_clear();
    } else if ((char)payload[0] == '1') {
      cfg.neopixel_en = true;
      publish_with_datetime(stat_topic, "NeoPixel: enabled");
    }
  }
  
  String res = "{\"neopixel_en\":";
  res += String(cfg.neopixel_en);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/neopixel/color" topic.
// Lights the NeoPixel LEDs in the given HEX color.
void cb_vl_neopixel_color(byte* payload, unsigned int len) {
  if (!cfg.neopixel_en) return;
  if (len != 7) return;
  
  np_set_color((char*)payload);
  publish_with_datetime(stat_topic, "NeoPixel: color set for all LEDs");
}

// A callback function for "vl/neopixel/sflash" topic.
// Flashes the NeoPixel LEDs in the given HEX color for 1 second.
void cb_vl_neopixel_sflash(byte* payload, unsigned int len) {
  if (!cfg.neopixel_en) return;
  if (len != 7) return;
  
  np_set_color((char*)payload);
  publish_with_datetime(stat_topic, "NeoPixel: flashing for 1 sec");
  delay(1000);
  np_clear();
}

// A callback function for "vl/neopixel/mflash" topic.
// Flashes the NeoPixel LEDs in the given HEX color for 3 seconds.
void cb_vl_neopixel_mflash(byte* payload, unsigned int len) {
  if (!cfg.neopixel_en) return;
  if (len != 7) return;
  
  np_set_color((char*)payload);
  publish_with_datetime(stat_topic, "NeoPixel: flashing for 3 sec");
  delay(3000);
  np_clear();
}

// A callback function for "vl/neopixel/lflash" topic.
// Flashes the NeoPixel LEDs in the given HEX color for 5 seconds.
void cb_vl_neopixel_lflash(byte* payload, unsigned int len) {
  if (!cfg.neopixel_en) return;
  if (len != 7) return;
  
  np_set_color((char*)payload);
  publish_with_datetime(stat_topic, "NeoPixel: flashing for 5 sec");
  delay(5000);
  np_clear();
}

// A callback function for "vl/neopixel/blink" topic.
// Blinks the NeoPixel LEDs in the given HEX color for the given time.
void cb_vl_neopixel_blink(byte* payload, unsigned int len) {
  char* buf = new char[len+1];
  strncpy(buf, (char*)payload, len);
  buf[len] = '\0';

  // The incoming message consists of two parts separated by a space.
  // The first part contains a HEX color and the second part a duration.
  // The whole message is stored at `buf`. The following code looks for
  // a space in the message, replaces it with a null terminator and sets
  // `sp` to point to the next character after the found space. Therefore,
  // the color is stored at `buf` and the duration at `sp`. 
  char* sp = strchr(buf, ' ');
  if (sp == NULL) { delete [] buf; return; }
  *sp = '\0';
  sp++;

  if (strlen(buf) != 7) { delete [] buf; return; }
  if (strlen(sp) < 1) { delete [] buf; return; }

  unsigned long duration = strtoul(sp, NULL, 10);
  if (duration == 0) { delete [] buf; return; }

  np_set_color(buf);
  publish_with_datetime(stat_topic, "NeoPixel: blinking");
  delay(duration);
  np_clear();

  delete [] buf;
}

// A callback function for "vl/neopixel/set" topic.
// Lights the given LED in the given HEX color.
void cb_vl_neopixel_set(byte* payload, unsigned int len) {
  char* buf = new char[len+1];
  strncpy(buf, (char*)payload, len);
  buf[len] = '\0';

  // The incoming message consists of two parts separated by a space.
  // The first part contains an index of the LED to light up and
  // the second part a HEX color.
  // See the comment in cb_vl_neopixel_blink() for more information.
  char* sp = strchr(buf, ' ');
  if (sp == NULL) { delete [] buf; return; }
  *sp = '\0';
  sp++;

  if (strlen(buf) < 1) { delete [] buf; return; }
  if (strlen(sp) != 7) { delete [] buf; return; }

  unsigned int led_id = strtoul(buf, NULL, 10);
  if (led_id >= NP_NUM) { delete [] buf; return; }

  char temp[3] = {0};
  strncpy(temp, &sp[1], 2);
  byte r = strtoul(temp, NULL, 16);
  strncpy(temp, &sp[3], 2);
  byte g = strtoul(temp, NULL, 16);
  strncpy(temp, &sp[5], 2);
  byte b = strtoul(temp, NULL, 16);

  np.setPixelColor(led_id, np.Color(r, g, b));
  np.show();

  String res = "NeoPixel: color set for LED";
  res += String(led_id);
  publish_with_datetime(stat_topic, res.c_str());

  delete [] buf;
}

// A callback function for "vl/neopixel/off" topic.
// Turns off the NeoPixel LEDs. 
void cb_vl_neopixel_off() {
  np_clear();
  publish_with_datetime(stat_topic, "NeoPixel: off");
}

// A callback function for "vl/buzzer/enable" topic.
// Sets the value of `cfg.buzzer_en` and/or publishes it to `info_topic`.
void cb_vl_buzzer_enable(byte* payload, unsigned int len) {
  if (len == 1) {
    if ((char)payload[0] == '0') {
      cfg.buzzer_en = false;
      publish_with_datetime(stat_topic, "Buzzer: disabled");
      buzzer_off();
    } else if ((char)payload[0] == '1') {
      cfg.buzzer_en = true;
      publish_with_datetime(stat_topic, "Buzzer: enabled");
    }
  }
  
  String res = "{\"buzzer_en\":";
  res += String(cfg.buzzer_en);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/buzzer/beep" topic.
// Beeps the buzzer for the given number of milliseconds.
void cb_vl_buzzer_beep(byte* payload, unsigned int len) {
  if (!cfg.buzzer_en) return;
  if (len <= 0) return;
  
  char* temp = new char[len+1];
  strncpy(temp, (char*)payload, len);
  temp[len] = '\0';
  unsigned long duration = strtoul(temp, NULL, 10);

  buzzer_on();
  publish_with_datetime(stat_topic, "Buzzer: beep!");
  delay(duration);
  buzzer_off();

  delete [] temp;
}

// A callback function for "vl/alarm/enable" topic.
// Sets the value of `cfg.alarm_en` and/or publishes it to `info_topic`.
void cb_vl_alarm_enable(byte* payload, unsigned int len) {
  if (len == 1) {
    if ((char)payload[0] == '0') {
      cfg.alarm_en = false;
      publish_with_datetime(stat_topic, "Alarm: disabled");
    } else if ((char)payload[0] == '1') {
      cfg.alarm_en = true;
      publish_with_datetime(stat_topic, "Alarm: enabled");
    }
  }
  
  String res = "{\"alarm_en\":";
  res += String(cfg.alarm_en);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/alarm/color1" topic.
// Sets the value of `cfg.color1` and/or publishes it to `info_topic`.
void cb_vl_alarm_color1(byte* payload, unsigned int len) {
  if (len == 7) {
    strncpy(cfg.color1, (char*)payload, 7);
    cfg.color1[7] = '\0';
    publish_with_datetime(stat_topic, "Color1: new value set");
  }

  String res = "{\"color1\":\"";
  res += String(cfg.color1);
  res += "\"}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/alarm/color2" topic.
// Sets the value of `cfg.color2` and/or publishes it to `info_topic`.
void cb_vl_alarm_color2(byte* payload, unsigned int len) {
  if (len == 7) {
    strncpy(cfg.color2, (char*)payload, 7);
    cfg.color2[7] = '\0';
    publish_with_datetime(stat_topic, "Color2: new value set");
  }

  String res = "{\"color2\":\"";
  res += String(cfg.color2);
  res += "\"}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/alarm/color3" topic.
// Sets the value of `cfg.color3` and/or publishes it to `info_topic`.
void cb_vl_alarm_color3(byte* payload, unsigned int len) {
  if (len == 7) {
    strncpy(cfg.color3, (char*)payload, 7);
    cfg.color3[7] = '\0';
    publish_with_datetime(stat_topic, "Color3: new value set");
  }

  String res = "{\"color3\":\"";
  res += String(cfg.color3);
  res += "\"}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/alarm/repetitions" topic.
// Sets the value of `cfg.alarm_repetitions` and/or publishes it to `info_topic`.
void cb_vl_alarm_repetitions(byte* payload, unsigned int len) {
  if (len > 0) {
    char* temp = new char[len+1];
    strncpy(temp, (char*)payload, len);
    temp[len] = '\0';
    cfg.alarm_repetitions = strtoul(temp, NULL, 10);
    publish_with_datetime(stat_topic, "Alarm: new value of `repetitions` set");
    delete [] temp;
  }
  
  String res = "{\"alarm_repetitions\":";
  res += String(cfg.alarm_repetitions);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/alarm/interval" topic.
// Sets the value of `cfg.alarm_interval` and/or publishes it to `info_topic`.
void cb_vl_alarm_interval(byte* payload, unsigned int len) {
  if (len > 0) {
    char* temp = new char[len+1];
    strncpy(temp, (char*)payload, len);
    temp[len] = '\0';
    cfg.alarm_interval = strtoul(temp, NULL, 10);
    publish_with_datetime(stat_topic, "Alarm: new value of `interval` set");
    delete [] temp;
  }
  
  String res = "{\"alarm_interval\":";
  res += String(cfg.alarm_interval);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/alarm/trigger" topic.
// Activates the appropriate alarm procedure depending on the message.
void cb_vl_alarm_trigger(byte* payload, unsigned int len) {
  char* trigger = new char[len+1];
  strncpy(trigger, (char*)payload, len);
  trigger[len] = '\0';

  if (strcmp(trigger, "opendoor") == 0) {
    mqttClient.publish(door_topic, "OPEN");
    publish_with_datetime(stat_topic, "Door: opened");
    
    if (cfg.alarm_en) {
      for (int i = 0; i < cfg.alarm_repetitions; ++i) {
        if (cfg.neopixel_en)
          np_set_color(cfg.color1);
        if (cfg.buzzer_en)
          buzzer_on();
        if (cfg.neopixel_en || cfg.buzzer_en)
          delay(cfg.alarm_interval);
        
        if (cfg.neopixel_en)
          np_set_color(cfg.color2);
        if (cfg.buzzer_en)
          buzzer_off();
        if (cfg.neopixel_en || cfg.buzzer_en)
          delay(cfg.alarm_interval);
      }
      np_clear();
      buzzer_off();
    }
    
    if (cfg.indicator_en)
      np_set_color(cfg.indicator_color);
  }

  else if (strcmp(trigger, "closedoor") == 0) {
    mqttClient.publish(door_topic, "CLOSED");
    publish_with_datetime(stat_topic, "Door: closed");
    
    if (cfg.alarm_en || cfg.indicator_en)
      np_clear();
    if (cfg.alarm_en)
      buzzer_off();
  }
  
  delete [] trigger;
}

// A callback function for "vl/opendoor/enable" topic.
// Sets the value of `cfg.indicator_en` and/or publishes it to `info_topic`.
void cb_vl_opendoor_enable(byte* payload, unsigned int len) {
  if (len == 1) {
    if ((char)payload[0] == '0') {
      cfg.indicator_en = false;
      publish_with_datetime(stat_topic, "Indicator: disabled");
    } else if ((char)payload[0] == '1') {
      cfg.indicator_en = true;
      publish_with_datetime(stat_topic, "Indicator: enabled");
    }
  }
  
  String res = "{\"indicator_en\":";
  res += String(cfg.indicator_en);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/opendoor/color" topic.
// Sets the value of `cfg.indicator_color` and/or publishes it to `info_topic`.
void cb_vl_opendoor_color(byte* payload, unsigned int len) {
  if (len == 7) {
    strncpy(cfg.indicator_color, (char*)payload, 7);
    cfg.indicator_color[7] = '\0';
    publish_with_datetime(stat_topic, "Indicator: new value of `color` set");
  }

  String res = "{\"indicator_color\":\"";
  res += String(cfg.indicator_color);
  res += "\"}";
  mqttClient.publish(info_topic, res.c_str());
}

// A callback function for "vl/config/load" topic.
// Loads the configuration from EEPROM.
void cb_vl_config_load() {
  byte* cfg_ptr = (byte*)&cfg;
  for (int i = 0; i < sizeof(struct configuration); ++i) {
    *cfg_ptr = EEPROM.read(i);
    cfg_ptr++;
  }
  Serial.println();
  Serial.println("Configuration loaded from EEPROM");
  publish_with_datetime(stat_topic, "|Configuration loaded from EEPROM|");
  delay(100);
}

// A callback function for "vl/config/store" topic.
// Stores the configuration in EEPROM.
void cb_vl_config_store() {
  byte* cfg_ptr = (byte*)&cfg;
  for (int i = 0; i < sizeof(struct configuration); ++i) {
    EEPROM.write(i, *cfg_ptr);
    cfg_ptr++;
  }
  Serial.println();
  if (EEPROM.commit()) {
    Serial.println("Configuration successfully stored in EEPROM");
    publish_with_datetime(stat_topic, "|Configuration successfully stored in EEPROM|");
  } else {
    Serial.println("Failed to store configuration in EEPROM");
    publish_with_datetime(stat_topic, "|Failed to store configuration in EEPROM|");
  }
  delay(100);
}

// A callback function for "vl/stat/interval" topic.
// Sets the value of `cfg.stat_interval` and/or publishes it to `info_topic`.
void cb_vl_stat_interval(byte* payload, unsigned int len) {
  if (len >= 4) {
    char* temp = new char[len+1];
    strncpy(temp, (char*)payload, len);
    temp[len] = '\0';
    cfg.stat_interval = strtoul(temp, NULL, 10);
    publish_with_datetime(stat_topic, "Status: new value of `interval` set");
    if (cfg.stat_interval < 1000) cfg.stat_interval = 1000;
    delete [] temp;
  }
  
  String res = "{\"stat_interval\":";
  res += String(cfg.stat_interval);
  res += "}";
  mqttClient.publish(info_topic, res.c_str());
}

void cb_vl_sleep(byte* payload, unsigned int len) {
  if (len <= 0) return;

  char* temp = new char[len+1];
  strncpy(temp, (char*)payload, len);
  temp[len] = '\0';
  unsigned long duration = strtoul(temp, NULL, 10);

  String res = "Sleep: ";
  res += String(duration);
  res += " ms";
  publish_with_datetime(stat_topic, res.c_str());
  delay(duration);

  delete [] temp;
}

// The main callback function that invokes specific callbacks based on the topic.
void callback(char* topic, byte* payload, unsigned int len) {
  Serial.println();
  Serial.println("Message received");
  Serial.print("T:[");
  Serial.print(topic);
  Serial.print("], M:[");
  for (int i = 0; i < len; ++i) {
    Serial.print((char)payload[i]);
  }
  Serial.println("]");

  if (strcmp(topic, "vl/neopixel/enable") == 0)
    cb_vl_neopixel_enable(payload, len);
  else if (strcmp(topic, "vl/neopixel/color") == 0)
    cb_vl_neopixel_color(payload, len);
  else if (strcmp(topic, "vl/neopixel/sflash") == 0)
    cb_vl_neopixel_sflash(payload, len);
  else if (strcmp(topic, "vl/neopixel/mflash") == 0)
    cb_vl_neopixel_mflash(payload, len);
  else if (strcmp(topic, "vl/neopixel/lflash") == 0)
    cb_vl_neopixel_lflash(payload, len);
  else if (strcmp(topic, "vl/neopixel/flash") == 0)
    cb_vl_neopixel_sflash(payload, len);
  else if (strcmp(topic, "vl/neopixel/blink") == 0)
    cb_vl_neopixel_blink(payload, len);
  else if (strcmp(topic, "vl/neopixel/set") == 0)
    cb_vl_neopixel_set(payload, len);
  else if (strcmp(topic, "vl/neopixel/off") == 0)
    cb_vl_neopixel_off();
  else if (strcmp(topic, "vl/buzzer/enable") == 0)
    cb_vl_buzzer_enable(payload, len);
  else if (strcmp(topic, "vl/buzzer/beep") == 0)
    cb_vl_buzzer_beep(payload, len);
  else if (strcmp(topic, "vl/alarm/enable") == 0)
    cb_vl_alarm_enable(payload, len);
  else if (strcmp(topic, "vl/alarm/color1") == 0)
    cb_vl_alarm_color1(payload, len);
  else if (strcmp(topic, "vl/alarm/color2") == 0)
    cb_vl_alarm_color2(payload, len);
  else if (strcmp(topic, "vl/alarm/color3") == 0)
    cb_vl_alarm_color3(payload, len);
  else if (strcmp(topic, "vl/alarm/repetitions") == 0)
    cb_vl_alarm_repetitions(payload, len);
  else if (strcmp(topic, "vl/alarm/interval") == 0)
    cb_vl_alarm_interval(payload, len);
  else if (strcmp(topic, "vl/alarm/trigger") == 0)
    cb_vl_alarm_trigger(payload, len);
  else if (strcmp(topic, "vl/opendoor/enable") == 0)
    cb_vl_opendoor_enable(payload, len);
  else if (strcmp(topic, "vl/opendoor/color") == 0)
    cb_vl_opendoor_color(payload, len);
  else if (strcmp(topic, "vl/config/load") == 0)
    cb_vl_config_load();
  else if (strcmp(topic, "vl/config/store") == 0)
    cb_vl_config_store();
  else if (strcmp(topic, "vl/stat/interval") == 0)
    cb_vl_stat_interval(payload, len);
  else if (strcmp(topic, "vl/sleep") == 0)
    cb_vl_sleep(payload, len);
  else
    ;
}

// A function that sets up the WiFi connection.
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to WiFi: ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  
  Serial.println("Connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

// A function that re-initiates a connection to the MQTT broker when it fails.
void reconnect() {
  while (!mqttClient.connected()) {
    Serial.println();
    Serial.println("Connecting to MQTT broker");
    String clientId = "ESP8266_";
    clientId += String(random(0xffff), HEX);

    if (mqttClient.connect(clientId.c_str())) {
      Serial.println("Connected");
      
      // Subscribe to the appropriate topics
      unsigned long topics_num = sizeof(topics) / sizeof(topics[0]);
      for (int i = 0; i < topics_num; ++i) {
        mqttClient.subscribe(topics[i]);
      }
    } else {
      Serial.print("Connection failed (RC=");
      Serial.print(mqttClient.state());
      Serial.println(")");
      Serial.println("Reconnecting in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {
  // Begin serial communication for debug messages
  Serial.begin(115200);

  // Initialize the buzzer and turn it off
  pinMode(BUZZ_PIN, OUTPUT);
  digitalWrite(BUZZ_PIN, HIGH);

  // Initialize the NeoPixel LEDs and turn them off
  np.begin();
  np.clear();
  np.show();

  // Initialize the WiFi communication
  setup_wifi();
  
  // Configure the MQTT broker and set a callback function
  mqttClient.setServer(broker, 1883);
  mqttClient.setCallback(callback);

  // Initiate a connection to the NTP server
  timeClient.begin();

  // Initialize the EEPROM and load the configuration
  EEPROM.begin(512);
  delay(10);
  cb_vl_config_load();
  
  // Set a seed for the pseudorandom number generator
  randomSeed(micros());
}

void loop() {
  // Connect to the MQTT broker if not already connected
  // or if the connection has been lost
  if (!mqttClient.connected()) {
    reconnect();
  }
  // Process incoming messages and maintain the connection to the server
  mqttClient.loop();

  // Update the time (every 12 h = 43200000 ms, see the NTPClient constructor)
  timeClient.update();

  // Send a status message every `stat_interval` ms
  unsigned long now = millis();
  if (now - last_stat > cfg.stat_interval) {
    last_stat = now;
    publish_with_datetime(stat_topic, "Status: OK");
  }
}
